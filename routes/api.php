<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ADMIN\CategoriaController;
use App\Http\Controllers\ADMIN\ProveController;
use App\Http\Controllers\ADMIN\ProductoController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('categoria')->group(function () {
    Route::get('/get',[ CategoriaController::class, 'getCategoria']);
    Route::post('/crear',[ CategoriaController::class, 'crear']);
    Route::post('/delete',[ CategoriaController::class, 'delete']);
    Route::get('/{id}',[ CategoriaController::class, 'get']);
    Route::put('/{id}',[ CategoriaController::class, 'editar']);


});
Route::prefix('proveedor')->group(function () {
    Route::get('/get',[ ProveController::class, 'getProve']);
    Route::post('/save',[ ProveController::class, 'add']);
    Route::get('/{id}',[ ProveController::class, 'get']);
    Route::put('/{id}',[ ProveController::class, 'editar']);
    Route::post('/delete',[ ProveController::class, 'delete']);

});
Route::prefix('productos')->group(function () {
    Route::post('/save',[ ProductoController::class, 'agregar']);
    Route::get('/getPro', [ProductoController::class, 'getPro']);
    Route::post('/getProName', [ProductoController::class, 'getProName']);

    Route::get('/{id}',[ ProductoController::class, 'get']);
    Route::put('/{id}',[ ProductoController::class, 'editar']);
    Route::put('/salida/{id}',[ ProductoController::class, 'editarSalida']);

});
