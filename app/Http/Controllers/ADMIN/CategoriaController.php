<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categoria;
use Log;

class CategoriaController extends Controller
{
    public function getCategoria(){
      try {
        $data = Categoria::where('deleted', 0)->get();
        return response()->json($data, 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }

    }

    public function get($id){
      try {
        $data = Categoria::find($id);
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }
    }

    public function crear(Request $request){
      try {
        $id = $request['id'];
        $data['cat_nombre'] = $request['nombre'];
        if($id>0){
          Categoria::find($id)->update($data);
        }else {
          Categoria::create($data);
        }
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }

    public function delete(Request $request){
      try {
          $id = $request['id'];
          Categoria::where('id', $request['id'])->update([
          'deleted'=> 1
        ]);
      } catch (\Exception $e) {

      }

    }


        public function editar(Request $request, $id){
          try {
              Log::info("entroooo");
              Log::info($request['nombre']);
              Log::info($id);
              $data['cat_nombre'] = $request['nombre'];
              Categoria::find($id)->update($data);

            return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
          } catch (\Exception $e) {
            return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

          }

        }

}
