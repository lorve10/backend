<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Acudiente;

class AcudienteController extends Controller
{
  public function getAll(){
  try {
    $data = Acudiente::get();
    return response()->json($data, 200);
  } catch (\Exception $e) {
    return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
  }


  }

  public function create(Request $request){
    try {
      $data['cedula'] = $request['cedula'];
      $data['nombre'] = $request['nombre'];
      $data['apellido'] = $request['apellido'];
      $data['parentesco'] = $request['parentesco'];
      $data['telefono'] = $request['telefono'];
      Acudiente::create($data);
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }
  }

  public function consulta(Request $request){
    Log::info($request);
    $data = Acudiente::where('nombre','=',$request['nombre'])->get();
    return response()->json($data, 200);
  }

  public function get($id){
    $data = Acudiente::find($id);
    return response()->json($data, 200);
  }

  public function update(Request $request,$id){
    try {
      $data['cedula'] = $request['cedula'];
      $data['nombre'] = $request['nombre'];
      $data['apellido'] = $request['apellido'];
      $data['parentesco'] = $request['parentesco'];
      $data['telefono'] = $request['telefono'];
      Acudiente::find($id)->update($data);
      return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }


  }

}
