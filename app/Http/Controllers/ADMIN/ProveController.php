<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Proveedor;
class ProveController extends Controller
{
  public function getProve(){
    try {
      $data = Proveedor::where('deleted',0)->get();
      return response()->json($data, 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
    }

  }
  public function get($id){
    try {
      $data = Proveedor::find($id);
      return response()->json(['message' => "Successfully loaded",'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }
  }

  public function editar(Request $request, $id){
    try {
        $data['pro_nombre'] = $request['nombre'];
        $data['pro_direccion'] = $request['direccion'];
        $data['pro_telefono'] = $request['telefono'];
        Proveedor::find($id)->update($data);
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }

  }

  public function add(Request $request){
    try {
        $data['pro_nombre'] = $request['nombre'];
        $data['pro_direccion'] = $request['direccion'];
        $data['pro_telefono'] = $request['telefono'];
        Proveedor::create($data);
        return response()->json(['message' => "Successfully loaded", 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }
  }

  public function delete(Request $request){
    try {
        $id = $request['id'];
        Proveedor::where('id', $request['id'])->update(['deleted'=> 1 ]);
      return response()->json(['message' => "Successfully loaded", 'success' => true ], 200);

    } catch (\Exception $e) {
      return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

    }

  }

}
