<?php

namespace App\Http\Controllers\ADMIN;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Productos;
use Log;


class ProductoController extends Controller
{
    public function agregar(Request $request){

      try {
          Log::info(json_encode($request['lista']));

          $lista = $request['lista'];

          foreach ($lista as $value) {
            $data['nombre'] = $value['nombre'];
            $data['precio'] = $value['precio'];
            $data['cantidad'] = $value['cantidad'];
            $data['id_categoria'] =$value['categoria'];
            $data['id_proveedor'] = $value['proveedor'];
            Productos::create($data);
        }
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }

    public function get($id){
      try {
        $data = Productos::find($id);
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }
    }


    public function getPro(){
      try {
        $res = 'SELECT productos.id, nombre, precio, cantidad, cat_nombre AS categoria, pro_nombre AS proveedor  FROM productos
                INNER JOIN categoria ON id_categoria = categoria.`id`
                INNER JOIN proveedores ON id_proveedor	= proveedores.`id`
                ';
        $data = DB::select($res);
        // $data = Productos::get();
        return response()->json($data, 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }
    public function getProName(Request $request){
      try {
        $dato = $request['nombre'];
        Log::info(gettype($dato));
        $res = ("SELECT productos.id, nombre, precio, cantidad, cat_nombre AS categoria, pro_nombre AS proveedor  FROM productos
                INNER JOIN categoria ON id_categoria = categoria.`id`
                INNER JOIN proveedores ON id_proveedor	= proveedores.`id`
                WHERE categoria.`cat_nombre` = '$dato'");
        $data = DB::select($res);
        // $data = Productos::get();
        return response()->json($data, 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);
      }
    }
    public function editar(Request $request, $id){
      try {
          $res = Productos::find($id);

          $cantidad = $request['cantidad'];
          $precio = $request['precio'];
          $data['precio'] = $res->precio + $precio;
          $data['cantidad'] = $res->cantidad + $cantidad;
          Productos::find($id)->update($data);
        return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }
    public function editarSalida(Request $request, $id){
      try {
          $res = Productos::find($id);
          Log::info($res);
          $cantidad = $request['cantidad'];
          $valorxcant = $res->precio/$res->cantidad;
          Log::info($valorxcant);
          if($cantidad > $res->cantidad){
             return response()->json(['message' => "Successfully loaded",  'success' => false ], 200);
          }
          else{
            $actualizar  = $res->cantidad-$cantidad;
            $precioActual = $valorxcant*$actualizar;
            $data['cantidad'] = $actualizar;
            $data['precio'] = $precioActual;
            Productos::find($id)->update($data);
            return response()->json(['message' => "Successfully loaded", 'data'=> $data, 'success' => true ], 200);
          }

      } catch (\Exception $e) {
        return response()->json([ 'message' => $e->getMessage(), 'success' => false ], 500);

      }

    }




}
