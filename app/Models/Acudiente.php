<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Acudiente extends Model
{
    use HasFactory;
    protected $table = "acudiente";

    protected $fillable = [
      'cedula',
      'nombre',
      'apellido',
      'parentesco',
      'telefono',
    ];
}
